﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TelemetrySimulator.Models;

namespace TelemetrySimulator.Services
{
    public class SimulationService
    {
        private static SimulationService _instance;
        
        private readonly KafkaProducerService _senderService;
        private readonly FrameData _frame;
        private CancellationTokenSource _token;
        
        private const int SIMULATION_INTERVAL = 70;

        private SimulationService()
        {
            _frame = new FrameData()
            {
                Counter = 0,
                Header = new Headers() { TailNumber = "210" },
                Parameters = new List<MessageElement>()
                {
                    new MessageElement() { ParameterName = "Alt", Value = 20 },
                    new MessageElement() { ParameterName = "Lat", Value = 20 }
                }

            };

            _senderService = KafkaProducerService.GetInstance();
        }

        public static SimulationService GetInstance()
        {
            return _instance ??= new SimulationService();
        }

        private void SimulateFrame(FrameData frameData)
        {
            frameData.Counter++;
            frameData.Header.Timestamp = DateTime.Now;
            frameData.Parameters.ForEach((parameter) => parameter.Value++);
        }


        public void Start()
        {
            _token = new CancellationTokenSource();
            Task.Run(() =>
            {
                while (!_token.IsCancellationRequested)
                {
                    SimulateFrame(_frame);
                    _senderService.ProduceData(_frame);
                    Console.WriteLine($"Frame generated at {_frame.Header.Timestamp}, {_frame.Counter}");
                    Thread.Sleep(SIMULATION_INTERVAL);
                }
            }, _token.Token);
        }

        public void Stop()
        {
            _token.Cancel();
        }
    }
}
