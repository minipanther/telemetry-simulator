﻿using System;

namespace TelemetrySimulator.Models
{
    public class Headers
    {
        public DateTime Timestamp { get; set; }
        public string TailNumber { get; set; }

        public Headers(string tailNumber = "000")
        {
            Timestamp = DateTime.Now;
            TailNumber = tailNumber;
        }
    }
}