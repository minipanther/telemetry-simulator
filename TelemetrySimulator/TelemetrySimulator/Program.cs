﻿using System;
using System.Threading;
using TelemetrySimulator.Services;

namespace TelemetrySimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            var simulationService = SimulationService.GetInstance();
            simulationService.Start();

            Console.ReadKey();
        }
    }
}
