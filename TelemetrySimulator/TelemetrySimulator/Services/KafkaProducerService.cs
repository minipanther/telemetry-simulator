﻿using Confluent.Kafka;
using System.Text.Json;
using System.Threading.Tasks;
using TelemetrySimulator.Models;

namespace TelemetrySimulator.Services
{
    public class KafkaProducerService
    {
        private const string USERNAME = "rwdadoqj";
        private readonly ProducerConfig _config;
        private readonly IProducer<string, string> _producer;

        private static KafkaProducerService _instance;

        public KafkaProducerService()
        {
            _config = new ProducerConfig
            {
                BootstrapServers = "dory.srvs.cloudkafka.com:9094",
                SaslUsername = $"{USERNAME}",
                SaslPassword = "i1A6Yx8wCvXx0npAflL3AK_mQQoe0sfY",
                SaslMechanism = SaslMechanism.ScramSha512,
                SecurityProtocol = SecurityProtocol.SaslSsl,
                Acks = Acks.All
            };

            _producer = new ProducerBuilder<string, string>(_config).Build();
        }

        public static KafkaProducerService GetInstance()
        {
            return _instance ??= new KafkaProducerService();
        }

        public void ProduceData(FrameData frame)
        {
            string jsonString = JsonSerializer.Serialize(frame);
            _producer.Produce($"{USERNAME}-{frame.Header.TailNumber}", new Message<string, string>() { Value = jsonString });
        }

        public Task ProduceDataAsync(FrameData frame)
        {
            string jsonString = JsonSerializer.Serialize(frame);
            return _producer.ProduceAsync($"{USERNAME}-{frame.Header.TailNumber}", new Message<string, string>() { Value = jsonString });
        }
    }
}
